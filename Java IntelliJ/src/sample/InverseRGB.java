package sample;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * Created by Guix on 30/04/2016.
 */
public class InverseRGB extends Filtre{


    public Image appliquer(Image img){
        int imageH = (int)img.getHeight()-1;
        int imageW = (int)img.getWidth()-1;
        WritableImage transform = new WritableImage(imageW+1,imageH+1);
        Color tempC = new Color(0,0,1,1.0);
        PixelReader pixelR = img.getPixelReader();
        PixelWriter pixelW = transform.getPixelWriter();

        double tempRed;
        double tempGreen;
        double tempBlue;

        for (int i =0;i<imageH;i++){ // height = y
            for(int j=0;j<imageW;j++){ // width = x

                tempC=pixelR.getColor(j , i); //on prend la couleur du pixel actuel

                //On récupère le RGB de la couleur prise ci dessus
                tempRed=tempC.getRed();
                tempGreen=tempC.getGreen();
                tempBlue=tempC.getBlue();

                //on crée une nouvelle couleur en inversant RGB pour GBR
                Color newColor=Color.color(tempGreen,tempBlue,tempRed);
                //On remplis le tableau
                pixelW.setColor(j, i, newColor);

            }
        }
        return transform;
    }

}
