package sample;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * Created by Guix on 30/04/2016.
 */
public abstract class Filtre {

    Image img = null;
    protected Color tabColorFiltre[][] = null;

    public Filtre(){

    }

    public abstract Image appliquer(Image img);


}
