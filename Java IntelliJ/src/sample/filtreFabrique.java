package sample;

import javafx.scene.image.Image;

/**
 * Created by Guix on 30/04/2016.
 */
public class filtreFabrique {
    //Cette classe va permetter de créer un Filtre

    // La création d'un Filtre en fonction de son type est encapsulée dans la fabrique.
    public Filtre creerFiltre(TypeFiltre typefiltre)
    {
        Filtre filtre = null;;
        switch(typefiltre)
        {
            case INVERSERGB:
                filtre = new InverseRGB();break;
            case NB:
                filtre = new NoirBlanc();break;
            case SEPIA:
                filtre = new Sepia();break;
        }
        return filtre;
    }

    // Enumération des filtres.
    public enum TypeFiltre
    {
        INVERSERGB,
        NB,
        SEPIA
    }
}
