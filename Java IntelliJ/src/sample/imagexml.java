package sample;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;


@XmlRootElement
public class imagexml {

    String path;
    ArrayList<String> tag=new ArrayList<String>();
    ArrayList<String> transformations=new ArrayList<String>();



    public void setPath(String path) {
        this.path = path;
    }

    @XmlElement
    public String getPath() {
        return path;
    }


    public void setTag(String tagToAdd) {
        this.tag.add(tagToAdd);
    }

    @XmlElement
    public ArrayList<String> getTag() {
        return this.tag;
    }

    public void setTransformations(String transToAdd) {
        this.transformations.add(transToAdd);
    }

    @XmlElement
    public ArrayList<String> getTransformations() {
        return this.transformations;
    }


}