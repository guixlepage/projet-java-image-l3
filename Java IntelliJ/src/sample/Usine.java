package sample;

import javafx.scene.image.Image;

/**
 * Created by Guix on 30/04/2016.
 */
public class Usine {
    private filtreFabrique filtreFabrique;// Attribut contenant la fabrique de Filtre

    // Le constructeur permet de sélectionner la fabrique à utiliser.
    public Usine()
    {
        this.filtreFabrique = new filtreFabrique();
    }

    // Méthode qui permet de construire l'ensemble des filtres.
    public Filtre faireFiltre(sample.filtreFabrique.TypeFiltre type)
    {
        Filtre filtre = this.filtreFabrique.creerFiltre(type);
        return filtre;
    }
}
