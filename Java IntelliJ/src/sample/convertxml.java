package sample;

import javafx.scene.image.Image;
import sun.plugin2.gluegen.runtime.StructAccessor;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class convertxml {

        imagexml xm = new imagexml();
        String nom;

    public convertxml(){}

    public convertxml(String name){
        xm.setPath(name);
    }

    public void saisirpath(String path, String name) {
        this.nom = name;
        xm.setPath(path);

        try {
            File file = new File("xml/"+this.nom+".xml");
            if (file.exists())  //si le fichier existe on l'ouvre
            {
                System.out.println("Cette image a déjà été ouverte : ouverture fichier xml");
            }
            else
            {
                JAXBContext jaxbContext = JAXBContext.newInstance(imagexml.class);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

                // output pretty printed
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

                jaxbMarshaller.marshal(xm, file);
                jaxbMarshaller.marshal(xm, System.out);
            }


        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }

    public void saisirtag(String tag) {


        xm.setTag(tag);


        try {

            File file = new File("xml/"+this.nom+".xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(imagexml.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(xm, file);
            jaxbMarshaller.marshal(xm, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }

    public void saisirtransfo(String transfo) {


        xm.setTransformations(transfo);


        try {

            File file = new File("xml/"+this.nom+".xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(imagexml.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(xm, file);
            jaxbMarshaller.marshal(xm, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }
}