package sample;

import com.sun.org.apache.xpath.internal.SourceTree;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;


import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Controller {

    public MenuButton transformations;
    public Button OPEN;
    public ImageView imgV;
    public Button setTag;
    public Button save;
    public Button savechang;
    public Button Open2;
    public TextField settagfield;
    public TextField searchtagfield;
    public ComboBox combo;
    public Button searchTag;
    public Button OK;
    public Image tmp;
    public String tmpname;
    public Stage stage;
    Usine usine = new Usine(); //Usine pour les filtres
    convertxml conv = new convertxml();
    ArrayList<String> transfoTemp = new ArrayList<String>();  // tableau temporaire qui sert a sauvegarder les transformations si l'utilisateur le souhaite

    public void open(ActionEvent event) {
        //FILECHOOSER open
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg"));
        String currentDir = System.getProperty("user.home");
        File defaultDirectory = new File(currentDir);
        fileChooser.setInitialDirectory(defaultDirectory);
        File selected = fileChooser.showOpenDialog(stage);
        if (selected != null) {
           // System.out.println(selected.getAbsolutePath()); // Affichage du path de l'image séléctionné
            try {
                 tmp = new Image(selected.toURI().toURL().toExternalForm()); //"file://"+selected.getAbsolutePath());
                this.conv.saisirpath(selected.getAbsolutePath(), selected.getName()); //On écrit dans le fichier xml de l'image son path
                this.tmpname = selected.getName();    // On utilise la variable tmpname qui correspond au nom de l'image (on test si elle existe déjà)
                imgV.setImage(tmp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else { // aucune sélection
            this.tmp = new Image("Resources/pie.jpg");
            this.conv.saisirpath("Resources/pie.jpg", "pie.jpg");
            imgV.setImage(tmp);
        }
    }

    public void save(ActionEvent event) {
        //FILECHOOSER SAVE
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg"));
        String currentDir = System.getProperty("user.home");
        File defaultDirectory = new File(currentDir);
        fileChooser.setInitialDirectory(defaultDirectory);
        File selected = fileChooser.showSaveDialog(stage);
        if (selected != null) {
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(tmp, null), "png", selected); //enregistrer le fichier
            } catch (Exception e) {
                e.printStackTrace();
            }
            imgV.setImage(tmp);
        }

    }


    public void symetrie(ActionEvent event) {
        WritableImage transform = new WritableImage((int) this.tmp.getWidth(), (int) this.tmp.getHeight());
        Color temp = new Color(0, 0, 0, 0);
        Color colorW = null;
        PixelReader pixelR = this.tmp.getPixelReader();
        PixelWriter pixelW = transform.getPixelWriter();
        int imageH = (int) this.tmp.getHeight() - 1;
        int imageW = (int) this.tmp.getWidth() - 1;
        for (int i = 0; i < imageH; i++) { // height = y
            for (int j = 0; j < imageW; j++) { // width = x

                temp = pixelR.getColor(j, i);
                pixelW.setColor(imageW - j, i, temp);

            }
        }
        tmp = transform;
        imgV.setImage(transform);
        this.transfoTemp.add("Symétrie");

    }

    public void RGB(ActionEvent event) {
        Image img;
        Filtre rgb = this.usine.faireFiltre(filtreFabrique.TypeFiltre.INVERSERGB);
        img = rgb.appliquer(tmp);
        tmp = img;
        imgV.setImage(img);
        this.transfoTemp.add("RGB");

    }

    public void noirblanc(ActionEvent event) {
        Image img;
        Filtre nb = this.usine.faireFiltre(filtreFabrique.TypeFiltre.NB);
        img = nb.appliquer(tmp);
        tmp = img;
        imgV.setImage(tmp);
        this.transfoTemp.add("Noir et Blanc");
    }

    public void sepia(ActionEvent event) {
        Image img;
        Filtre sepia = this.usine.faireFiltre(filtreFabrique.TypeFiltre.SEPIA);
        img = sepia.appliquer(tmp);
        tmp = img;
        imgV.setImage(img);
        this.transfoTemp.add("Sepia");
    }

    public void sobel(ActionEvent event) {

    }

    public void appliHisto(ActionEvent event) {
        File file = new File("xml/" + tmpname +".xml");
        if (file.exists())
        {
            this.conv = new convertxml("xml/" + tmpname +".xml");
           try {
               DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
               DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
               Document doc = dBuilder.parse(file);
               NodeList nList = doc.getElementsByTagName("transformations"); //récupère la liste des transformations
               for(int count=0;count<nList.getLength();count++){
                   System.out.println(nList.item(count).getTextContent());
                   switch (nList.item(count).getTextContent()) {
                       case "RGB":
                           RGB(event);
                           break;
                       case "Sepia":
                           sepia(event);
                           break;
                       case "Noir et Blanc":
                           noirblanc(event);
                           break;
                       case "Symétrie":
                           symetrie(event);
                           break;
                       case "Sobel":
                           sobel(event);
                           break;
                       default:
                           System.out.println("debug");
                           break;
                   }
               }
           }
           catch(Exception e){};

        }

    }

    public void savechang(ActionEvent event) {
        int taille = this.transfoTemp.size();

        //delete pour eviter duplication
        while(transfoTemp.size()>0){
            this.conv.saisirtransfo(this.transfoTemp.remove(0));
        }
    }

    public void settag(ActionEvent event) {
        this.conv.saisirtag(settagfield.getText());
    }

    public void searchTag(ActionEvent event) {
        String tagRecherche = searchtagfield.getText();
        String path ="xml";

        List<String> listefichiers = new ArrayList<String>();
        File[] fichiers = new File("xml").listFiles();

        for (File file : fichiers) {
            if (file.isFile()) {
                try {
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(file);
                    NodeList nList = doc.getElementsByTagName("tag"); //récupère la liste des tag
                    for(int count=0;count<nList.getLength();count++){
                        // System.out.println(nList.item(count).getTextContent());
                        if(tagRecherche.equals(nList.item(count).getTextContent())){
                            listefichiers.add(file.getName()); //si le tag correspond a la recherche on ajoute le xml de ce fichier a listefichiers
                        }
                    }

                }
                catch(Exception e){
                    e.printStackTrace();
                    System.out.println("error");
                };

            }

        }

        //http://docs.oracle.com/javafx/2/ui_controls/combo-box.htm
        combo.getItems().removeAll(combo.getItems()); //clear la combobox
        for (int i =0; i<listefichiers.size() ; i++) {
           // System.out.println(listefichiers.get(i));  //On récupère les éléments de la liste de fichier qui comporte le tag souhaité
            combo.getItems().add(listefichiers.get(i)); //On les ajoutes dans la combo box
        }
        if(listefichiers == null){
            combo.setValue(listefichiers.get(0));
        }

    }

    public void ok(ActionEvent event) {
        try {
        String fich = (String)combo.getSelectionModel().getSelectedItem().toString(); //on récupère la valeur selectionnée par l'utilisateur
       // System.out.println(fich);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse("xml/" + fich);
            NodeList nList = doc.getElementsByTagName("path");
          //  System.out.println("path image : " + nList.item(0).getTextContent());  //on cherche le path de l'image séléctionnée
            this.tmp= new Image("file://" + nList.item(0).getTextContent()); // on l'ouvre
            this.imgV.setImage(this.tmp); // affichage
        }catch(Exception e){
        e.printStackTrace();
        System.out.println("ERREUR OUVERTURE IMAGE");
    };
    }
}
